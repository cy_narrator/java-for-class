
/* Loop from 1 to 100
    If num is divisible by 5, print "Div5"
        If num is divisible by 3, print "Div3"
        If num is divisible by both 3 and 5, print "Div35"
else print the number
*/

package com.ems.namfiz;

/**
 *
 * @author user
 */
public class NamFiz {

   public static void main(String[] args) {
       int i;
       
       for(i=0; i<=100; i++){
           if(i % 3 == 0 && i % 5 ==0){
               System.out.println("Div35");
           } else if(i % 3 == 0) {
               System.out.println("Div3");
           }else if(i % 5 ==0){
               System.out.println("Div5");
           } else {
               System.out.println(i);
           }
       }

    }
}

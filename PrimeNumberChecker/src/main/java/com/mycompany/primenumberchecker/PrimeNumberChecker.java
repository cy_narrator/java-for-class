/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.primenumberchecker;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class PrimeNumberChecker {
    private static boolean is_prime(int num)
    {
        int flag = 1;
        /** num is converted into positive just so single loop can work
         * for both positive and negative value of num
         */
        if(num < 0)
        {
            flag = -1; //Just so to note it is negative
            num *= flag; //Any number multiplied by -1 makes it negative
        }
        if(num < 4)
        {
            if(num < 1)
            {
                // This catches for 0
                System.out.println(num+" is not considered prime");
                return false;
            }
            else
            {
                // This catches for 1, 2 and 3
                return true;
            }
        }
        else if(num % 2 == 0)
        { //When it comes to this else if, we can be certain it is equal or greater than 4
            System.out.println((num * flag) + " is an even number meaning it is not prime");
           return false; 
        }
        else
        { //At this point only odd numbers remain
            int num_halved = (int)(num / 2) + 1;
            for(int i = 3; i <= num_halved; i += 2)
            {
                if(num % i == 0)
                {
                    System.out.println((num * flag) + " is divisible by " + i + " times " + (num / i) * flag + " meaning it is not prime");
                    return false;
                }
            }
            return true;
        }
        
    }
    public static void main(String[] args) {
        System.out.print("Enter the number to check prime: ");
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        if(is_prime(num))
        {
            System.out.println("It is a prime number");
        }
    }
}

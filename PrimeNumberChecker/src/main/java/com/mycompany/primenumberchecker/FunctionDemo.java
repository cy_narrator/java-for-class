package com.mycompany.primenumberchecker;
public class FunctionDemo {
    public int doSum(int number_one, int number_two){
        int result = number_one + number_two;
        return result;
    }
   
    public String getDetail(){
        String name = "CITE";
        String location = "Tinkune";
        String detail = "Name: " + name + " Location: " + location;
        return detail;
    }
    
    public void calculateArea(double length, double breadth){
        double area = length * breadth;
        System.out.println("Area of rectangle: " + area);
    }
    
    public void showQuote(){
        String quote = "Dont build a castle in air";
        System.out.println(quote);
    }
    
    public static void main(String args[]){
        FunctionDemo fd = new FunctionDemo();
        System.out.println("Sum of two number:" + fd.doSum(12, 23));
    
        System.out.println("Detail: " + fd.getDetail());
        
        fd.calculateArea(23, 12);
    }
    
}

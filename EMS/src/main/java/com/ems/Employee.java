package com.ems;

public class Employee {
    
    // attributes
    String first_name;
    String middle_name;
    String last_name;
    String contact;
    String dob;
    String email;
    String password;
    
    // non parameter
    Employee(){
        this.first_name = "Ram";
        this.middle_name = "Prasad";
        this.last_name = "Prasai";
        this.dob = "2000-02-22";
        this.email = "ramp@gmail.com";    
    }
    
    // parameter
    Employee(String first_name, String last_name, String email){
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;        
    }
    
    Employee(String email){
        this.email = email;
    }
    
    Employee(String email, String password){
        this.email = email;
        this.password = password;
    }
    
    public void showEmployeeDetail(){
        System.out.println("First Name: " + this.first_name);
    }
    
}

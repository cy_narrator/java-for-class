package com.ems;

public class EMS {

    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.showEmployeeDetail();
        
//        object creation via parameterize constructor
        Employee emp_obj = new Employee("Muna", "Guragain", "muna@gmail.com");
        System.out.println("First Name:" + emp_obj.first_name);
        System.out.println("Email: " + emp_obj.email);
        
        Employee emp_login = new Employee("harinara@gmail.com", "hari123");
        if (emp_login.email.equals("harinara@gmail.com")){
            if (emp_login.password.equals("hari123")){
                System.out.println("Login Success");
            }else{
                System.out.println("Invalid Email or Password");
            }
        }else{
            System.out.println("Unauthorized Access");
        }
    }
}

package com.mycompany.gpacalculator;
import java.util.Scanner;
/**
 *
 * @author user
 */
public class GPACalculator {
    private static double range_limit(double obtained_marks, double full_marks, int range)
    {
        return (obtained_marks / full_marks) * range;
    }
    
    private static String give_grade(double percentage)
    {
        String grades[] = {"Not Graded", "F", "D", "D+", "C", "C+", "B", "B+", "A", "A+"};
        int index = (int) percentage / grades.length;
        return grades[index];

    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double full_marks = 100.0;
        double marks; //Just declare that there will be a variable
        do
        {
            System.out.print("Enter the obtained Marks: ");
            marks = scanner.nextDouble();
        } while(marks < 0 || marks > 100);
        double percentage = range_limit(marks, full_marks, 100);
        double gpa = range_limit(marks, full_marks, 4);
        System.out.println("Your Percentage is "+percentage+", gpa is "+gpa+" and grade is "+give_grade(percentage));
        
    }
}

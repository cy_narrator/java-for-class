package inheritance;

public class InheritanceMain {
    public static void main(String args[]){
        Dog dog = new Dog();
        dog.bark();
        
        dog.walk();
        dog.eat();
        dog.breathe();
     
        Cat cat = new Cat();
        cat.meow();
        
        cat.eat();
        cat.walk();
        cat.breathe();
        
        
        // multi-level 
        Model samsung = new Model();
        samsung.call();
        samsung.message();
        samsung.play();
        
        samsung.series();
        samsung.info();
        
        Rectangle rect = new Rectangle();
        rect.base = 12;
        rect.breadth = 13;
        rect.height = 8;
        rect.length = 14;
        
        rect.calculateArea("rectangle");
    }
}

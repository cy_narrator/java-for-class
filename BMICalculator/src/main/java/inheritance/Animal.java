package inheritance;

public class Animal {
    public void walk(){
        System.out.println("Animal can walk");
    }
    
    public void breathe(){
        System.out.println("Animal can breathe");
    }
    
    public void eat(){
        System.out.println("Animal can eat");
    }
}

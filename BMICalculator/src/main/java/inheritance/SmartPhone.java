package inheritance;

public class SmartPhone {
    public void call(){
        System.out.println("I can call");
    }
    
    public void message(){
        System.out.println("I can message");
    }
    
    public void play(){
        System.out.println("I can play music and video");
    }
}

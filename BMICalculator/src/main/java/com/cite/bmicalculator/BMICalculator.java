package com.cite.bmicalculator;

import com.cite.bmicalculator.model.User;

public class BMICalculator {

    public static void main(String[] args) {
        User user = new User();
        user.name = "Hari Narayan";
        user.address = "Kathmandu";
        
        System.out.println(user.name);
        System.out.println(user.address);
        
        user.setDob("2022");
        System.out.println(user.getDob());
        String email = "hari@gmail.com";
        user.setEmail(email);
        System.out.println(user.getEmail());
    }
}

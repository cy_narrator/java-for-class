package com.cite.bmicalculator.model;

public class User {
    // public attributes
    public String name;
    public String address;
    
    // protected attributes
    protected String dob;
    protected String contact;
    
    // private attributes
    private String email;
    
    
    public String getDob(){
        return this.dob;
    }
    
    public void setDob(String param_dob){
        this.dob = param_dob;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    public void setEmail(String param_email){
        this.email = param_email;
    }
    
    public void registerUser(){}   
    public void login(){}   
    public void checkAvailability(){}    
    public void userProfile(){}   
    public void changePassword(){}
    public void forgetPassword(){}
   
    
}

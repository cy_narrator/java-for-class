package abstractionandinterface;

public class Car extends Vehicle{

    @Override
    public void brake() {
        System.out.println("I am a braking system for car");
    }
    
}

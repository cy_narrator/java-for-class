package abstractionandinterface;

public class Circle implements Shape{
    public double radius = 12;
    public double pi = 3.1428;
    
    @Override
    public void area() {
        double result = this.pi*(this.radius*this.radius);
        System.out.println("Area of circle: " + result);
    }

    @Override
    public void perimeter() {
        double result = 2*this.pi*this.radius;
        System.out.println("Perimeter of circle: " + result);
    }
    
}

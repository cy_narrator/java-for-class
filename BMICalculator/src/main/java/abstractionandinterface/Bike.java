package abstractionandinterface;

public class Bike extends Vehicle{

    @Override
    public void brake() {
        System.out.println("I am a braking system of bike");
    }
    
}

package abstractionandinterface;

public class Main {
    public static void main(String[] args) {
        Vehicle v1 = new Bike();
        v1.brake();
        
        Vehicle v2 = new Car();
        v2.brake();
        
        
        Shape s1 = new Circle();
        s1.area();
        s1.perimeter();
        
        Shape s2 = new Rectangle();
        s2.area();
        s2.perimeter();
    }
}

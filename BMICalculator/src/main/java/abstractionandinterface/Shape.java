package abstractionandinterface;

public interface Shape {
    public abstract void area();
    public abstract void perimeter();
}

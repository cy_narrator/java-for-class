package abstractionandinterface;

public class Rectangle implements Shape{
    public double length = 12;
    public double breadth = 9;
    
    @Override
    public void area() {
        double result = this.length * this.breadth;
        System.out.println("Area of rectangle: " + result);
    }

    @Override
    public void perimeter() {
        double result = 2 * (this.length + this.breadth);
        System.out.println("Perimeter of rectangle: " + result);
    }
    
}

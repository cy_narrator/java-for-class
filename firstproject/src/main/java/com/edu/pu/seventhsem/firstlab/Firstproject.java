package com.edu.pu.seventhsem.firstlab;
// single comments
/**
 * multiple line comments
 */

/**
 *
 * @author Utsav
 */
class Firstproject {
    public static void main(String[] args) {
        // datatype and operator examples
        // case - Order Price DEtails
        
        String first_name = "Hari";
        String last_name = "Narayan";
        String contact = "+977 9876543210";
        String email = "harin@gmail.com";
        
        String items[] = {"1kg Potato", "Half KG Daal"};
        double item_one_price = 1 * 100;
        double item_two_price = 1/2*200;
        float total_price = (float)(item_one_price + item_two_price);
        
        String bill_no = "REF 2022";
        String order_date = "2022-02-12";
        
        
    }
}

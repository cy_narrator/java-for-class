package com.edu.pu.seventhsem.firstlab;

public class ControlStatementDemo {
    public static void main(String args[]){
    String name = "CITE";
    String location = "Tinkune";
    String phone = "9787";
    int total_student = 1230;
    
    // if statement
    // use if there is only one statement
    if ("Tinkune".equals(location) && "CITE".equals(name)){
        System.out.println("Correct");
    }
    
    // if else statement
    // if condition is not true and we want to execute alternative
    // operation
    
    if (total_student == 122){
        System.out.println("Total student:" + total_student);
    }else{
        System.out.println("Total student:" + total_student);
    }
    
    // if else if statement
    // to check one condition multiple time
    
    String day_name = "Tuesday";
    if (day_name.equals("Sunday")){
        System.out.println(day_name + " is a first day of week");
    } else if (day_name.equals("Monday")){
        System.out.println(day_name + " is a second day of week");
    } else if (day_name.equals("Tuesday")){
        System.out.println(day_name + " is a third day of week");
    }
    
    // nested if statement
    // if inside if
    String username = "citeuser";
    String password = "cite123";
    if (username.equals("citeuser")){
        if (password.equals("cite123")){
            System.out.println("Login Success");
        }else{
            System.out.println("Invalid username or password");
        }
    }else{
        System.out.println("Unauthorized Access");
    }
    
    // Result marking system
    double obtained_marks = 66.78;
    if ((obtained_marks > 0) && (obtained_marks <= 39.99)){
        System.out.println("Fail");
    }else if ((obtained_marks > 40) && (obtained_marks <= 59.99)){
        System.out.println("Second Division");
    }
    } 
}

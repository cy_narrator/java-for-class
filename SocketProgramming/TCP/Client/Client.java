package Client;
import java.net.*;
import java.io.*;
import java.util.*;
public class Client
{
    public static void main(String[] args)
    {
        // Server needs to be running before client runs
        String host = "127.0.0.1"; // Can be either "localhost" or "127.0.0.1" If both client and server are to run in the same computer
        int port = 12345; // Any unused port number, but make sure both client and server uses the same port
        /* Define everything we use and set to null */
        Socket client_socket = null;
        InputStreamReader input_stream_reader = null;
        OutputStreamWriter output_stream_writer = null;
        BufferedReader buffered_reader = null;
        BufferedWriter buffered_writer = null;


        try
        {
            //Creating a new socket
            client_socket = new Socket(host, port);
        
            /* We use streams to communicate over the socket.
            * There are two types of streams,
            * 1: Byte Stream that we use to send data like images, videos, audio
            * 2: Character Stream that we use to send text characters
            */
            
            /* In Java, Character Stream class name end with the name 'Reader' or 'Writer'
            * That means if you are using a class name InputStreamReader you are using Character Stream to read
            */
            
            // Define Stream Reader and Writer
            input_stream_reader = new InputStreamReader(client_socket.getInputStream()); // To read one character at a time from the server
            output_stream_writer = new OutputStreamWriter(client_socket.getOutputStream()); // To write one character at a time to the server
            
            /* The problem is we cannot just send one character at a time to the server.
            * Internet is slow and that causes issues
            * So we use BufferedReader/BufferedWriter to group a bunch of characters and send them at once
            * We will be wrapping our InputStreamReader and OutputStreamWriter in BufferedReader and BufferedWriter
            */
            buffered_reader = new BufferedReader(input_stream_reader);
            buffered_writer = new BufferedWriter(output_stream_writer);

            /* At this point we already have our connection ready to go and now we need to send and receive data */
            Scanner scanner = new Scanner(System.in); // To read data from the console to send the other side

            while(true)
            {
                /* We do while(true) because are continously sending and receiving data from the server */
                System.out.println("Enter the message to be sent: ");
                String message_to_send = scanner.nextLine();
                buffered_writer.write(message_to_send); // We send data to the server
                buffered_writer.newLine(); // The way socket works, it does not know the message has ended and so we send a newline to tell it that the current message is over
                buffered_writer.flush(); // We may not have actually sent all the message above if the buffer is not full. So we have to force send all the buffer contents

                /* We now handle received message */
                String received_message = buffered_reader.readLine(); // This will wait till the server sends message and saves it to received_message variable
                System.out.println("Server:   "+ received_message); // Display the message
                
                /* Close socket if the message received is BYE */
                if(received_message.equals("BYE"))
                {
                    System.out.println("Client terminating...");
                    client_socket.close();
                    break;
                }
            }

        }
        catch(Exception exp)
        {
            /* It is a good idea to wrap the entire socket code into a try block */
            exp.printStackTrace();
        }
    }
}
package Server;
import java.net.*;
import java.io.*;
import java.util.*;

public class Server {
    public static void main(String[] args)
    {
        // Server needs to be running before client runs
        // Server does not need to specify host, just needs port
        int port = 12345; // Any unused port number, but make sure both client and server uses the same port

        /* We define everything we will use and set it to null */
        ServerSocket server_socket = null;
        Socket specific_client_socket = null;
        InputStreamReader input_stream_reader = null;
        OutputStreamWriter output_stream_writer = null;
        BufferedReader buffered_reader = null;
        BufferedWriter buffered_writer = null;

        // Creating a new ServerSocket
        /* Unlike in C, in Java we have a clear distinction between client and server socket */
        try
        {
            server_socket = new ServerSocket(port); // This is a port we define server to listen on
            /* A client comes and goes but server will always be there */
            while(true)
            {
                /* This while true block will make it such that server will always be running and waiting for a client */
                specific_client_socket = server_socket.accept(); // When a client comes along, it will create a new socket object to handle that one client.

                /* Server will just be hung up till a client appears meaning whatever line below runs only after the client comes */
                /* After this we use our InputStreamReader and Buffers like we did in client but with specific_client_socket instead of server_socket */
                input_stream_reader = new InputStreamReader(specific_client_socket.getInputStream());
                output_stream_writer = new OutputStreamWriter(specific_client_socket.getOutputStream());
                buffered_reader = new BufferedReader(input_stream_reader);
                buffered_writer = new BufferedWriter(output_stream_writer);
                Scanner scanner = new Scanner(System.in);
                while(true)
                {
                    /* This inner while loop is to send and receive message to that one specific client */
                    /* Client will be the one to first send message */
                    String received_message = buffered_reader.readLine();
                    System.out.println("Client:   " + received_message);
                    if(received_message.equals("BYE"))
                    {
                        System.out.println("The current client wishes to close");
                        specific_client_socket.close();
                    }

                    System.out.println("Enter the message to send");
                    String message_to_send = scanner.nextLine();
                    buffered_writer.write(message_to_send); // We send data to the server
                    buffered_writer.newLine(); // The way socket works, it does not know the message has ended and so we send a newline to tell it that the current message is over
                    buffered_writer.flush(); // We may not have actually sent all the message above if the buffer is not full. So we have to force send all the buffer contents
                }


            }

        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.atmmachine;

/**
 *
 * @author user
 */
public class ATMMachine {
    
    public static String show_message(int id)
    {
        String message[] = {"Welcome", "Please Enter Your card", "Please enter the PIN", "Transaction complete", "Thank you for banking with us"};
        return message[id % message.length];
    }

    public static void main(String[] args) {
        for(int i=0; i < 10; i++)
        {
            System.out.println(show_message(i));
        }
        System.out.println("Hello World!");
    }
}
